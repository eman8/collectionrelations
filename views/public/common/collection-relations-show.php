<div id="collection-relations-display-collection-relations">
    <?php if (!$subjectRelations && !$objectRelations): ?>
    <p><?php echo "Cette collection n'a pas de relation indiquée avec une autre collection du projet."; ?></p>
    <?php else: ?>
    <table>
    		<?php $previous_relation = ""; ?>
    		<?php foreach ($subjectRelations as $subjectRelation): ?>
	        <?php if ($previous_relation <> $subjectRelation['relation_text']) { ?>
		        <tr>
<!--                 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>  		         -->
                <?php $intitule = get_option('cr_intitule', 'Cette collection');	?>
		        		<td><?php echo __($intitule);?>
	            	<span title="<?php echo html_escape($subjectRelation['relation_description']); ?>" style="font-style:italic;"><?php echo strip_tags($subjectRelation['relation_text']); ?> : </span>
	            	</td>
		     		</tr>
          <?php } ?>

		     	<tr>
<!--               <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
	            <td>
	            	<?php // echo $subjectRelation['collection_thumbnail']; ?>
	            	<a style='line-height:20px;' href="<?php echo url('collections/show/' . $subjectRelation['object_collection_id']); ?>"><?php echo $subjectRelation['object_collection_title']; ?></a>
	            	<br /><div class='relation-comment' style='clear:both;background:#ddd;font-style:italic;'><?php echo $subjectRelation['relation_comment']; ?></div>
	            </td>
	            </tr>
		     	<?php
					  $previous_relation = $subjectRelation['relation_text'];
          endforeach; ?>
          <tr><td></td></tr>
    		<?php $previous_relation = ""; ?>
    		<?php foreach ($objectRelations as $objectRelation): ?>
	        <?php if ($previous_relation <> $objectRelation['relation_text']) { ?>
		     	<?php $previous_relation = $objectRelation['relation_text'];} ?>
	        <tr>
<!--               <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>   -->
	            <td><a style='line-height:20px;' href="<?php echo url('collections/show/' . $objectRelation['subject_collection_id']); ?>"><?php echo $objectRelation['subject_collection_title']; ?></a>
	            </td>
	            </tr>
		        <tr>
<!--                 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>  		         -->
                <?php $intitule = get_option('cr_intitule_obj');
                  $intitule ? null : $intitule = 'cette collection';	?>
		        		<td><span title="<?php echo html_escape($objectRelation['relation_description']); ?>" style="font-style:italic;"><?php echo $objectRelation['relation_text']; echo '  ' . $intitule; ?> </span>
<div class='relation-comment' style='clear:both;background:#ddd;font-style:italic;'><?php echo $objectRelation['relation_comment']; ?></div>
	            	</td>
		     		</tr>
		     	<?php $previous_relation = ""; ?>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>
